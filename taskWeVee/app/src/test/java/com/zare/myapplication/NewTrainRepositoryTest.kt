package com.zare.myapplication

import com.zare.myapplication.data.model.EngineType
import com.zare.myapplication.data.model.Locomotive
import com.zare.myapplication.data.model.Train
import com.zare.myapplication.data.model.Wagon
import com.zare.myapplication.data.model.WagonType
import com.zare.myapplication.data.repository.NewTrainRepository
import org.junit.Assert.*
import org.junit.Test

class NewTrainRepositoryTest {

    @Test
    fun testCalculateTotalEmptyWeight() {

        val locomotive1 = Locomotive(
            "Loco1",
            "Manufacturer1",
            2020,
            "123",
            500.0,
            10.0,
            EngineType.DIESEL,
            1000.0,
            50,
            2000.0
        )
        val locomotive2 = Locomotive(
            "Loco2",
            "Manufacturer2",
            2021,
            "456",
            600.0,
            12.0,
            EngineType.ELECTRIC,
            1200.0,
            60,
            2500.0
        )
        val wagon1 =
            Wagon("Wagon1", "Manufacturer3", 2019, "789",
                200.0, WagonType.DINING, 8.0, 40, 1000.0)
        val wagon2 = Wagon(
            "Wagon2",
            "Manufacturer4",
            2022,
            "012",
            300.0,
            WagonType.PASSENGER,
            10.0,
            50,
            1200.0
        )

        val train = Train(
            id = 1,
            locomotives = mutableListOf(locomotive1, locomotive2),
            wagons = mutableListOf(wagon1, wagon2)
        )


        val repository = NewTrainRepository(listOf(train))
        val result = repository.getTotalEmptyWeight(1)
        assertEquals(1600.0, result!!, 0.01)

    }

    @Test
    fun testGetMaxPassengerCapacity() {
        val locomotive1 = Locomotive(
            "Loco1",
            "Manufacturer1",
            2020,
            "123",
            500.0,
            10.0,
            EngineType.DIESEL,
            1000.0,
            50,
            2000.0
        )
        val locomotive2 = Locomotive(
            "Loco2",
            "Manufacturer2",
            2021,
            "456",
            600.0,
            12.0,
            EngineType.ELECTRIC,
            1200.0,
            60,
            2500.0
        )
        val wagon1 =
            Wagon("Wagon1", "Manufacturer3", 2019,
                "789", 200.0, WagonType.DINING, 8.0, 40, 1000.0)
        val wagon2 = Wagon(
            "Wagon2",
            "Manufacturer4",
            2022,
            "012",
            300.0,
            WagonType.PASSENGER,
            10.0,
            50,
            1200.0
        )

        val train = Train(
            id = 1,
            locomotives = mutableListOf(locomotive1, locomotive2),
            wagons = mutableListOf(wagon1, wagon2)
        )


        val repository = NewTrainRepository(listOf(train))

        val result = repository.getMaxPassengerCapacity(1)
        assertEquals(200, result)
    }

    @Test
    fun testGetMaxGoodsWeight() {
        val locomotive1 = Locomotive(
            "Loco1",
            "Manufacturer1",
            2020,
            "123",
            500.0,
            10.0,
            EngineType.DIESEL,
            1000.0,
            50,
            2000.0
        )
        val locomotive2 = Locomotive(
            "Loco2",
            "Manufacturer2",
            2021,
            "456",
            600.0,
            12.0,
            EngineType.ELECTRIC,
            1200.0,
            60,
            2500.0
        )
        val wagon1 =
            Wagon("Wagon1", "Manufacturer3", 2019,
                "789", 200.0, WagonType.DINING, 8.0, 40, 1000.0)
        val wagon2 = Wagon(
            "Wagon2",
            "Manufacturer4",
            2022,
            "012",
            300.0,
            WagonType.PASSENGER,
            10.0,
            50,
            1200.0
        )

        val train = Train(
            id = 1,
            locomotives = mutableListOf(locomotive1, locomotive2),
            wagons = mutableListOf(wagon1, wagon2)
        )


        val repository = NewTrainRepository(listOf(train))

        val result = repository.getMaxGoodsWeight(1)
        assertEquals(4500.0, result!!, 0.01)
    }

    @Test
    fun testCalculateMaxLoadingForTrain() {

        val trainId = 1
        val mockTrain = Train(
            id = trainId,
            locomotives = mutableListOf(
                Locomotive(
                    "Locomotive1",
                    "Manufacturer1",
                    2022,
                    "12345",
                    100.0,
                    10.0,
                    EngineType.ELECTRIC,
                    200.0,
                    50,
                    500.0
                )
            ),
            wagons = mutableListOf(
                Wagon(
                    "Wagon1",
                    "Manufacturer2",
                    2022,
                    "67890",
                    50.0,
                    WagonType.PASSENGER,
                    8.0,
                    30,
                    300.0
                )
            )
        )


        val trainService = NewTrainRepository(listOf(mockTrain))

        val result = trainService.getMaxLoadingForTrain(trainId)
        assertEquals(6500.0, result!!, 0.01)

    }

    @Test
    fun testGetMaxTotalWeight() {
        val locomotive1 = Locomotive(
            "Loco1",
            "Manufacturer1",
            2020,
            "123",
            500.0,
            10.0,
            EngineType.DIESEL,
            1000.0,
            50,
            2000.0
        )
        val locomotive2 = Locomotive(
            "Loco2",
            "Manufacturer2",
            2021,
            "456",
            600.0,
            12.0,
            EngineType.ELECTRIC,
            1200.0,
            60,
            2500.0
        )
        val wagon1 =
            Wagon("Wagon1", "Manufacturer3", 2019, "789",
                200.0, WagonType.DINING, 8.0, 40, 1000.0)
        val wagon2 = Wagon(
            "Wagon2",
            "Manufacturer4",
            2022,
            "012",
            300.0,
            WagonType.PASSENGER,
            10.0,
            50,
            1200.0
        )

        val train = Train(
            id = 1,
            locomotives = mutableListOf(locomotive1, locomotive2),
            wagons = mutableListOf(wagon1, wagon2)
        )


        val repository = NewTrainRepository(listOf(train))

        val result = repository.getcalculateMaxTotalWeight(1)
        assertEquals(21100.0, result!!, 0.01)

    }

    @Test
    fun testAddLocomotiveToTrain() {

        val trainId = 1
        val locomotive1 = Locomotive(
            "Loco1",
            "Manufacturer1",
            2020,
            "123",
            500.0,
            10.0,
            EngineType.DIESEL,
            1000.0,
            50,
            2000.0
        )
        val train =
            Train(id = trainId, locomotives = mutableListOf(locomotive1), wagons = mutableListOf())

        val trainService = NewTrainRepository(listOf(train))

        trainService.addLocomotiveToTrain(trainId, locomotive1)

        val updatedTrain = trainService.getTrainById(trainId)

        assertEquals(mutableListOf(locomotive1), updatedTrain?.locomotives)
    }

    @Test
    fun testaddWagonToTrain() {

        val trainId = 1
        val wagon = Wagon(
            "Wagon1",
            "Manufacturer2",
            2022,
            "67890",
            50.0,
            WagonType.PASSENGER,
            8.0,
            30,
            300.0
        )
        val train =
            Train(id = trainId, locomotives = mutableListOf(), wagons = mutableListOf(wagon))

        val trainService = NewTrainRepository(listOf(train))

        trainService.addWagonToTrain(trainId, wagon)

        val updatedTrain = trainService.getTrainById(trainId)

        assertEquals(mutableListOf(wagon), updatedTrain?.wagons)
    }

    @Test
    fun testRemoveLocomotiveFromTrain() {

        val trainId = 1
        val locomotive1 = Locomotive(
            "Loco1",
            "Manufacturer1",
            2020,
            "123",
            500.0,
            10.0,
            EngineType.DIESEL,
            1000.0,
            50,
            2000.0
        )
        val locomotive2 = Locomotive(
            "Loco2",
            "Manufacturer2",
            2023,
            "323",
            200.0,
            10.0,
            EngineType.DIESEL,
            1000.0,
            20,
            1000.0
        )

        val train = Train(
            id = trainId,
            locomotives = mutableListOf(locomotive1, locomotive2),
            wagons = mutableListOf()
        )

        val trainService = NewTrainRepository(listOf(train))

        trainService.addLocomotiveToTrain(trainId, locomotive1)
        trainService.addLocomotiveToTrain(trainId, locomotive2)

        trainService.removeLocomotiveFromTrain(trainId, locomotive1)

        val updatedTrain = trainService.getTrainById(trainId)

        assertEquals(mutableListOf(locomotive2), updatedTrain?.locomotives)

    }

    @Test
    fun testRemoveWagonFromTrain() {

        val trainId = 1
        val wagones1 = Wagon(
            "Wagon1",
            "Manufacturer1",
            2022,
            "67890",
            50.0,
            WagonType.PASSENGER,
            8.0,
            30,
            300.0
        )
        val wagones2 = Wagon(
            "Wagon2",
            "Manufacturer2",
            2012,
            "67890",
            10.0,
            WagonType.PASSENGER,
            8.0,
            30,
            100.0
        )

        val train = Train(
            id = trainId,
            locomotives = mutableListOf(),
            wagons = mutableListOf(wagones1, wagones2)
        )

        val trainService = NewTrainRepository(listOf(train))

        trainService.addWagonToTrain(trainId, wagones1)
        trainService.addWagonToTrain(trainId, wagones2)

        trainService.removeWagonFromTrain(trainId, wagones1)

        val updatedTrain = trainService.getTrainById(trainId)

        assertEquals(mutableListOf(wagones2), updatedTrain?.wagons)

    }

    @Test
    fun testIsTrainCapableOfDriving() {

        val locomotive1 = Locomotive(
            "Loco1",
            "Manufacturer1",
            2020,
            "123",
            500.0,
            10.0,
            EngineType.DIESEL,
            10000.0,
            50,
            2000.0
        )
        val locomotive2 = Locomotive(
            "Loco2",
            "Manufacturer2",
            2023,
            "323",
            200.0,
            10.0,
            EngineType.DIESEL,
            10000.0,
            20,
            1000.0
        )

        val train1 =
            Train(id = 1, locomotives = mutableListOf(locomotive1), wagons = mutableListOf())
        val trainService = NewTrainRepository(listOf(train1))

        assertTrue(trainService.isTrainCapableOfDriving(1))
        assertFalse(trainService.isTrainCapableOfDriving(2))
        assertFalse(trainService.isTrainCapableOfDriving(3))
    }

    @Test
    fun getTrainLength() {
        val locomotive1 = Locomotive(
            "Loco1",
            "Manufacturer1",
            2020,
            "123",
            500.0,
            10.0,
            EngineType.DIESEL,
            1000.0,
            50,
            2000.0
        )
        val locomotive2 = Locomotive(
            "Loco2",
            "Manufacturer2",
            2021,
            "456",
            500.0,
            12.0,
            EngineType.ELECTRIC,
            1200.0,
            60,
            2500.0
        )
        val wagon1 =
            Wagon("Wagon1", "Manufacturer3", 2019, "789", 500.0, WagonType.GOODS, 8.0, 40, 1000.0)
        val wagon2 =
            Wagon("Wagon2", "Manufacturer4", 2022, "012", 500.0, WagonType.GOODS, 10.0, 50, 1200.0)

        val train1 = Train(
            id = 1,
            locomotives = mutableListOf(locomotive1, locomotive2),
            wagons = mutableListOf(wagon1, wagon2)
        )
        val trainService = NewTrainRepository(listOf(train1))

        assertEquals(40.0, trainService.getTrainLength(1)!!, 0.01)
    }

    @Test
    fun isWagonAssignedToOtherTrain_false() {
        val trainId = 1
        val wagones1 = Wagon(
            "Wagon1",
            "Manufacturer1",
            2022,
            "67890",
            50.0,
            WagonType.PASSENGER,
            8.0,
            30,
            300.0
        )
        val wagones2 = Wagon(
            "Wagon2",
            "Manufacturer2",
            2012,
            "67890",
            10.0,
            WagonType.PASSENGER,
            8.0,
            30,
            100.0
        )

        val train =
            Train(id = trainId, locomotives = mutableListOf(), wagons = mutableListOf(wagones1))

        val trainService = NewTrainRepository(listOf(train))

        assertEquals(trainService.isWagonAssignedToOtherTrain(wagones2), false)
    }

    @Test
    fun isWagonAssignedToOtherTrain_true() {
        val trainId = 1
        val wagones1 = Wagon(
            "Wagon1",
            "Manufacturer1",
            2022,
            "67890",
            50.0,
            WagonType.PASSENGER,
            8.0,
            30,
            300.0
        )

        val train =
            Train(id = trainId, locomotives = mutableListOf(), wagons = mutableListOf(wagones1))

        val trainService = NewTrainRepository(listOf(train))

        assertEquals(trainService.isWagonAssignedToOtherTrain(wagones1), true)
    }

    @Test
    fun isLocomotiveAssignedToOtherTrain_false() {
        val trainId = 1
        val locomotive1 = Locomotive(
            "Loco1",
            "Manufacturer1",
            2020,
            "123",
            500.0,
            10.0,
            EngineType.DIESEL,
            1000.0,
            50,
            2000.0
        )
        val locomotive2 = Locomotive(
            "Loco2",
            "Manufacturer2",
            2021,
            "456",
            500.0,
            12.0,
            EngineType.ELECTRIC,
            1200.0,
            60,
            2500.0
        )

        val train =
            Train(id = trainId, locomotives = mutableListOf(locomotive1), wagons = mutableListOf())

        val trainService = NewTrainRepository(listOf(train))

        assertEquals(trainService.isLocomotiveAssignedToOtherTrain(locomotive2), false)
    }

    @Test
    fun isLocomotiveAssignedToOtherTrain_true() {
        val trainId = 1

        val locomotive = Locomotive(
            "Loco2",
            "Manufacturer2",
            2021,
            "456",
            500.0,
            12.0,
            EngineType.ELECTRIC,
            1200.0,
            60,
            2500.0
        )

        val train =
            Train(id = trainId, locomotives = mutableListOf(locomotive), wagons = mutableListOf())

        val trainService = NewTrainRepository(listOf(train))

        assertEquals(trainService.isLocomotiveAssignedToOtherTrain(locomotive), true)
    }

    @Test
    fun isConductorNeeded_true() {
        val trainId = 1

        val locomotive = Locomotive(
            "Loco2",
            "Manufacturer2",
            2021,
            "456",
            500.0,
            12.0,
            EngineType.ELECTRIC,
            1200.0,
            60,
            2500.0
        )
        val wagon =
            Wagon("Wagon1", "Manufacturer3", 2019, "789",
                500.0, WagonType.GOODS, 8.0, 30, 1000.0)

        val train = Train(
            id = trainId,
            locomotives = mutableListOf(locomotive),
            wagons = mutableListOf(wagon)
        )

        val trainService = NewTrainRepository(listOf(train))

        assertEquals(trainService.isConductorNeeded(train), true)
    }

    @Test
    fun isConductorNeeded_false() {
        val trainId = 1

        val locomotive = Locomotive(
            "Loco2",
            "Manufacturer2",
            2021,
            "456",
            500.0,
            12.0,
            EngineType.ELECTRIC,
            1200.0,
            0,
            2500.0
        )
        val wagon =
            Wagon("Wagon1", "Manufacturer3", 2019, "789",
                500.0, WagonType.GOODS, 8.0, 0, 1000.0)

        val train = Train(
            id = trainId,
            locomotives = mutableListOf(locomotive),
            wagons = mutableListOf(wagon)
        )

        val trainService = NewTrainRepository(listOf(train))

        assertEquals(trainService.isConductorNeeded(train), false)
    }

    @Test
    fun getMaxConductorsNeeded() {
        val trainId = 1

        val locomotive = Locomotive(
            "Loco2",
            "Manufacturer2",
            2021,
            "456",
            500.0,
            12.0,
            EngineType.ELECTRIC,
            1200.0,
            2,
            2500.0
        )
        val wagon =
            Wagon("Wagon1", "Manufacturer3", 2019, "789",
                500.0, WagonType.GOODS, 8.0, 2, 1000.0)

        val train = Train(
            id = trainId,
            locomotives = mutableListOf(locomotive),
            wagons = mutableListOf(wagon)
        )

        val trainService = NewTrainRepository(listOf(train))

        assertEquals(trainService.getMaxConductorsNeeded(trainId), 1)
    }


    @Test
    fun changeTrainComposition() {
        val trainId = 1

        val locomotive1 = Locomotive(
            "Loco1",
            "Manufacturer1",
            2020,
            "123",
            500.0,
            10.0,
            EngineType.DIESEL,
            1000.0,
            50,
            2000.0
        )
        val wagon1 =
            Wagon(
                "Wagon1", "Manufacturer3",
                2019,
                "789", 500.0,
                WagonType.GOODS, 8.0, 40, 1000.0
            )

        val train = Train(
            id = trainId,
            locomotives = mutableListOf(locomotive1),
            wagons = mutableListOf(wagon1)
        )

        val newLocomotives = listOf(
            Locomotive(
                "Loco2", "Manufacturer2",
                2021, "456", 500.0,
                12.0, EngineType.ELECTRIC, 1200.0,
                60, 2500.0
            )
        )
        val newWagons = listOf(
            Wagon(
                "Wagon2", "Manufacturer4",
                2022, "012", 500.0, WagonType.GOODS,
                10.0, 50, 1200.0
            )

        )

        val trainService = NewTrainRepository(listOf(train))

        trainService.changeTrainComposition(trainId, newLocomotives, newWagons)

        val updatedTrain = trainService.getTrainById(trainId)

        assertEquals(newLocomotives, updatedTrain?.locomotives)
        assertEquals(newWagons, updatedTrain?.wagons)
    }

    @Test
    fun testHasCycles() {
        val wagon1 = (
                Wagon(
                    "Wagon1",
                    "Manufacturer2",
                    2022,
                    "67890",
                    50.0,
                    WagonType.PASSENGER,
                    8.0,
                    30,
                    300.0
                )
                )

        val wagon2 = (
                Wagon(
                    "Wagon2",
                    "Manufacturer2",
                    2022,
                    "67890",
                    50.0,
                    WagonType.DINING,
                    81.0,
                    10,
                    300.0
                )
                )

        val wagon3 = (
                Wagon(
                    "Wagon3",
                    "Manufacturer2",
                    2022,
                    "67890",
                    50.0,
                    WagonType.GOODS,
                    21.0,
                    0,
                    300.0
                )
                )
        wagon1.connectedWagons = listOf(wagon2)
        wagon2.connectedWagons = listOf(wagon3)
        wagon3.connectedWagons = listOf(wagon1)

        val trainId = 1
        val train = Train(
            id = trainId,
            locomotives = mutableListOf(),
            wagons = mutableListOf(wagon1, wagon2, wagon3)
        )
        val trainService = NewTrainRepository(listOf(train))

        val hasCycles = trainService.hasCycles(trainId)

        assertTrue(hasCycles)
    }
}

