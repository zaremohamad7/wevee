package com.zare.myapplication.data.model

class Wagon(
    designation: String,
    manufacturer: String,
    yearOfProduction: Int,
    serialNumber: String,
    emptyWeight: Double,
    val wagonType: WagonType,
     length: Double,
    val maxPassengers: Int,
    val maxLoadingWeight: Double,
    var connectedWagons: List<Wagon> = emptyList()
) : TrainElement(designation, manufacturer, yearOfProduction, serialNumber, emptyWeight, length)