package com.zare.myapplication.data.model

data class Train(
    val id: Int,
    var locomotives: MutableList<Locomotive>,
    var wagons: MutableList<Wagon>
)