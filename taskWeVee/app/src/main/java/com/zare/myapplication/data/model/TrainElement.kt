package com.zare.myapplication.data.model

abstract class TrainElement(
    val designation: String,
    val manufacturer: String,
    val yearOfProduction: Int,
    val serialNumber: String,
    val emptyWeight: Double,
    val length: Double
)
