package com.zare.myapplication.data.repository

import com.zare.myapplication.data.model.Locomotive
import com.zare.myapplication.data.model.Train
import com.zare.myapplication.data.model.Wagon
import com.zare.myapplication.data.model.WagonType

class NewTrainRepository(private val trains: List<Train>) {

    fun getTrainById(trainId: Int): Train? {
        return trains.find { it.id == trainId }
    }

    fun getTotalEmptyWeight(trainId: Int): Double? {
        val train = getTrainById(trainId)
        return train?.let { calculateTotalEmptyWeight(it) }
    }

    fun getMaxPassengerCapacity(trainId: Int): Int? {
        val train = getTrainById(trainId)
        return train?.let { calculateMaxPassengerCapacity(it) }
    }

    fun getMaxGoodsWeight(trainId: Int): Double? {
        val train = getTrainById(trainId)
        return train?.let { calculateMaxGoodsWeight(it) }
    }

    fun getMaxLoadingForTrain(trainId: Int): Double? {
        val train = getTrainById(trainId)
        return train?.let { calculateMaxLoadingForTrain(it) }
    }

    fun getMaxTotalWeight(trainId: Int): Double? {
        val train = getTrainById(trainId)
        return train?.let { calculateMaxLoadingForTrain(it) }
    }

    fun getcalculateMaxTotalWeight(trainId: Int): Double? {
        val train = getTrainById(trainId)
        return train?.let { calculateMaxTotalWeight(it) }
    }

    fun isTrainCapableOfDriving(trainId: Int): Boolean {
        val train = getTrainById(trainId)
        return train?.let {
            val maxTotalWeight = getMaxTotalWeight(trainId) ?: 0.0
            val tractiveEffort = it.locomotives.sumByDouble { loco -> loco.tractiveEffort }
            tractiveEffort >= maxTotalWeight
        } ?: false
    }

    fun isConductorNeeded(train: Train): Boolean {
        val maxPassengerCapacity = getMaxPassengerCapacity(train)
        val passengersPerConductor = 50
        return maxPassengerCapacity > 0 && (maxPassengerCapacity / passengersPerConductor) != 0
    }

    fun getMaxConductorsNeeded(trainId: Int): Int? {
        val train = getTrainById(trainId)
        return train?.let { calculateConductorsNeeded(it) }
    }

    fun isLocomotiveAssignedToOtherTrain(locomotive: Locomotive): Boolean {
        return trains.any { it.locomotives.contains(locomotive) }
    }

    fun isWagonAssignedToOtherTrain(wagon: Wagon): Boolean {
        return trains.any { it.wagons.contains(wagon) }
    }

    fun changeTrainComposition(
        trainId: Int,
        newLocomotives: List<Locomotive>,
        newWagons: List<Wagon>
    ) {
        val train = getTrainById(trainId)
        train?.let {
            it.locomotives.clear()
            it.wagons.clear()
            it.locomotives.addAll(newLocomotives)
            it.wagons.addAll(newWagons)
        }
    }

    fun getTrainLength(trainId: Int): Double? {
        val train = getTrainById(trainId)
        return train?.let { calculateTrainLength(it) }
    }

    fun addLocomotiveToTrain(trainId: Int, locomotive: Locomotive) {
        val train = getTrainById(trainId)
        train?.let {
            if (!it.locomotives.contains(locomotive)) {
                it.locomotives.add(locomotive)
            }
        }
    }

    fun addWagonToTrain(trainId: Int, wagon: Wagon) {
        val train = getTrainById(trainId)
        train?.let {
            if (!it.wagons.contains(wagon)) {
                it.wagons.add(wagon)
            }
        }
    }

    fun removeLocomotiveFromTrain(trainId: Int, locomotive: Locomotive) {
        val train = getTrainById(trainId)
        train?.locomotives?.remove(locomotive)
    }

    fun removeWagonFromTrain(trainId: Int, wagon: Wagon) {
        val train = getTrainById(trainId)
        train?.wagons?.remove(wagon)
    }

    fun hasCycles(trainId: Int): Boolean {
        val train = getTrainById(trainId)
        return train?.let {
            val visitedLocomotives = mutableSetOf<Any>()
            val visitedWagons = mutableSetOf<Any>()

            val hasCyclesInLocomotives = hasCycles(it.locomotives, visitedLocomotives)
            val hasCyclesInWagons =
                hasCycles(it.wagons, visitedWagons) || hasCyclesBetweenWagons(it.wagons)

            hasCyclesInLocomotives || hasCyclesInWagons
        } ?: false
    }

    private fun hasCyclesBetweenWagons(wagons: List<Wagon>): Boolean {
        val visitedWagons = mutableSetOf<Wagon>()
        for (wagon in wagons) {
            if (hasCyclesBetweenWagons(wagon, visitedWagons)) {
                return true
            }
        }
        return false
    }

    private fun hasCyclesBetweenWagons(wagon: Wagon, visitedWagons: MutableSet<Wagon>): Boolean {
        if (visitedWagons.contains(wagon)) {
            return true
        }
        visitedWagons.add(wagon)

        for (connectedWagon in wagon.connectedWagons) {
            if (hasCyclesBetweenWagons(connectedWagon, visitedWagons)) {
                return true
            }
        }

        visitedWagons.remove(wagon)
        return false
    }

    private fun hasCycles(elements: List<Any>, visited: MutableSet<Any>): Boolean {
        for (element in elements) {
            if (visited.contains(element)) {
                return true
            }
            visited.add(element)
            if (element is Train) {
                if (hasCycles(element.locomotives + element.wagons, visited)) {
                    return true
                }
            }
        }
        return false
    }

    fun getMaxPassengerCapacity(train: Train): Int {
        val sumPassengerCapacityLocomotives = train.locomotives.sumBy { it.maxPassengers }
        val sumPassengerCapacityWagons = train.wagons.sumBy { it.maxPassengers }
        return sumPassengerCapacityLocomotives + sumPassengerCapacityWagons
    }


    fun calculateConductorsNeeded(train: Train): Int {
        val maxPassengerCapacity = getMaxPassengerCapacity(train)
        return if (maxPassengerCapacity > 0) (maxPassengerCapacity / 50) + if (maxPassengerCapacity % 50 > 0) 1 else 0 else 0
    }


    fun calculateMaxLoadingForTrain(train: Train): Double {
        val maxPassengerWeight = getMaxPassengerCapacity(train) * 75.0
        val maxGoodsWeight = calculateMaxGoodsWeight(train)
        return maxPassengerWeight + maxGoodsWeight
    }

    private fun calculateMaxTotalWeight(train: Train): Double {
        val totalEmptyWeight = calculateTotalEmptyWeight(train)
        val maxLoadingForTrain = calculateMaxLoadingForTrain(train)
        return totalEmptyWeight + maxLoadingForTrain
    }

    private fun calculateTrainLength(train: Train): Double {
        val locomotiveLength = train.locomotives.sumByDouble { it.length }
        val wagonLength = train.wagons.sumByDouble { it.length }
        return locomotiveLength + wagonLength
    }

    private fun calculateTotalEmptyWeight(train: Train): Double {
        val locomotiveWeight = train.locomotives.sumByDouble { it.emptyWeight }
        val wagonWeight = train.wagons.sumByDouble { it.emptyWeight }
        return locomotiveWeight + wagonWeight
    }

    private fun calculateMaxPassengerCapacity(train: Train): Int {
        val maxPassengerCapacityLocomotives = train.locomotives.sumBy { it.maxPassengers }
        val maxPassengerCapacityWagons = train.wagons.sumBy { it.maxPassengers }
        return maxPassengerCapacityLocomotives + maxPassengerCapacityWagons
    }

    fun calculateMaxGoodsWeight(train: Train): Double {
        val maxGoodsWeightLocomotives = train.locomotives.sumByDouble { it.maxLoadingWeight }

        val maxGoodsWeightWagons = train.wagons
            .filter { it.wagonType == WagonType.GOODS }
            .toList()
            .sumByDouble { it.maxLoadingWeight }

        return maxGoodsWeightLocomotives + maxGoodsWeightWagons
    }
}