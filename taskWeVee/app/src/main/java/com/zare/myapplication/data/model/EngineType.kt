package com.zare.myapplication.data.model

enum class EngineType {
    DIESEL,
    STEAM,
    ELECTRIC
}