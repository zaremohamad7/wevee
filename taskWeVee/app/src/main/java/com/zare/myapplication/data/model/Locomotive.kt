package com.zare.myapplication.data.model

class Locomotive(
    designation: String,
    manufacturer: String,
    yearOfProduction: Int,
    serialNumber: String,
    emptyWeight: Double,
    length: Double,
    val engineType: EngineType,
    val tractiveEffort: Double,
    val maxPassengers: Int,
    val maxLoadingWeight: Double
) : TrainElement(designation, manufacturer, yearOfProduction, serialNumber, emptyWeight, length)

