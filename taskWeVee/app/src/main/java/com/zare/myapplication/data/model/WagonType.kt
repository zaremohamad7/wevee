package com.zare.myapplication.data.model

enum class WagonType {
    PASSENGER,
    SLEEPING,
    DINING,
    GOODS
}